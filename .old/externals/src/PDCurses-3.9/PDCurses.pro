#-------------------------------------------------
#
# Project created by QtCreator 2019-09-23T15:30:54
#
#-------------------------------------------------

QT       -= core gui

TEMPLATE = lib

DEFINES += \
    PDC_DLL_BUILD \
    CURSES_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG(debug, debug|release) {
    win32:TARGET = PDCursesWin32d
    DESTDIR = ./debug
} else {
    win32:TARGET = PDCursesWin32
    DESTDIR = ./release
}
SOURCES += \
    pdcurses/addch.c \
    pdcurses/addchstr.c \
    pdcurses/addstr.c \
    pdcurses/attr.c \
    pdcurses/beep.c \
    pdcurses/bkgd.c \
    pdcurses/border.c \
    pdcurses/clear.c \
    pdcurses/color.c \
    pdcurses/debug.c \
    pdcurses/delch.c \
    pdcurses/deleteln.c \
    pdcurses/getch.c \
    pdcurses/getstr.c \
    pdcurses/getyx.c \
    pdcurses/inch.c \
    pdcurses/inchstr.c \
    pdcurses/initscr.c \
    pdcurses/inopts.c \
    pdcurses/insch.c \
    pdcurses/insstr.c \
    pdcurses/instr.c \
    pdcurses/kernel.c \
    pdcurses/keyname.c \
    pdcurses/mouse.c \
    pdcurses/move.c \
    pdcurses/outopts.c \
    pdcurses/overlay.c \
    pdcurses/pad.c \
    pdcurses/panel.c \
    pdcurses/printw.c \
    pdcurses/refresh.c \
    pdcurses/scanw.c \
    pdcurses/scr_dump.c \
    pdcurses/scroll.c \
    pdcurses/slk.c \
    pdcurses/termattr.c \
    pdcurses/touch.c \
    pdcurses/util.c \
    pdcurses/window.c \
#    dos/pdcclip.c \
#    dos/pdcdisp.c \
#    dos/pdcgetsc.c \
#    dos/pdckbd.c \
#    dos/pdcscrn.c \
#    dos/pdcsetsc.c \
#    dos/pdcutil.c \
#    os2/pdcclip.c \
#    os2/pdcdisp.c \
#    os2/pdcgetsc.c \
#    os2/pdckbd.c \
#    os2/pdcscrn.c \
#    os2/pdcsetsc.c \
#    os2/pdcutil.c \
#    sdl1/pdcclip.c \
#    sdl1/pdcdisp.c \
#    sdl1/pdcgetsc.c \
#    sdl1/pdckbd.c \
#    sdl1/pdcscrn.c \
#    sdl1/pdcsetsc.c \
#    sdl1/pdcutil.c \
#    sdl1/sdltest.c \
#    sdl2/pdcclip.c \
#    sdl2/pdcdisp.c \
#    sdl2/pdcgetsc.c \
#    sdl2/pdckbd.c \
#    sdl2/pdcscrn.c \
#    sdl2/pdcsetsc.c \
#    sdl2/pdcutil.c \
#    sdl2/sdltest.c \
#    x11/pdcclip.c \
#    x11/pdcdisp.c \
#    x11/pdcgetsc.c \
#    x11/pdckbd.c \
#    x11/pdcscrn.c \
#    x11/pdcsetsc.c \
#    x11/pdcutil.c \
#    x11/sb.c \
#    x11/scrlbox.c

HEADERS += \
    common/acs437.h \
    common/acsgr.h \
    common/acsuni.h \
    common/font437.h \
    common/iconbmp.h \
    curses.h \
    curspriv.h \
    panel.h \
#	dos/pdcdos.h \
#	os2/pdcos2.h \
#	sdl1/pdcsdl.h \
#	sdl2/pdcsdl.h \
#	x11/pdcx11.h \
#	x11/scrlbox.h

win32 {
    LIBS += \
        "c:\Program Files (x86)\Windows Kits\8.1\Lib\winv6.3\um\x64\User32.Lib" \
        "c:\Program Files (x86)\Windows Kits\8.1\Lib\winv6.3\um\x64\AdvAPI32.Lib"

    SOURCES += \
        wincon/pdcclip.c \
        wincon/pdcdisp.c \
        wincon/pdcgetsc.c \
        wincon/pdckbd.c \
        wincon/pdcscrn.c \
        wincon/pdcsetsc.c \
        wincon/pdcutil.c

    HEADERS += \
        wincon/pdcwin.h
}

unix {
    target.path = /usr/lib
    INSTALLS += target
}
