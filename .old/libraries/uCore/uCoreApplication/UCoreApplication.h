#ifndef UCOREAPPLICATION_H
#define UCOREAPPLICATION_H

#include <QObject>
#include <QtWidgets/QApplication>
#include <QCommandLineParser>

#include "../uCoreTerminal/UCoreTerminal.h"

namespace uCore {
namespace application {

class UCoreApplication : public QObject
{
    Q_OBJECT
public:
    explicit UCoreApplication(QObject *parent = nullptr);
    ~UCoreApplication();
    int initializeCoreApplication(int &argc, char **argv[]);

    QCommandLineParser* getCommandLineParser() { return m_pCommandLineParser; }

    //  variables

protected:

private:
    void createApplication(int &argc, char **argv[]);
    void coreApplication();
    void setCommandLineOptions(QCommandLineParser* parser);
    int guiApplication();
    int terminalApplication();

    virtual QString applicationName() = 0;
    virtual QString applicationVersion() = 0;
    virtual QString organisationName() = 0;

    virtual void setAdditionalCommandLineOptions(QCommandLineParser* parser) { Q_UNUSED(parser) }
    virtual void startGuiApplication() = 0;
    virtual void startTerminalApplication() = 0;

    //  variables
    static UCoreApplication* s_instance;
    QStringList m_qstrlArguments;
    QCommandLineParser* m_pCommandLineParser;
    terminal::UCoreTerminal* m_pTerminal;

signals:

public slots:
};

}   //  namespace application
}   //  namespace uCore

#endif // UCOREAPPLICATION_H
