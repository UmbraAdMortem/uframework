#include "UCoreApplication.h"

#include "../uCoreLogger/UCoreLogger.h"

using namespace uCore::application;

UCoreApplication* UCoreApplication::s_instance = nullptr;

UCoreApplication::UCoreApplication(QObject *parent)
    : QObject(parent) {
    if(!s_instance)
        s_instance = this;
}

UCoreApplication::~UCoreApplication()
{
    m_pCommandLineParser->~QCommandLineParser();

    QCoreApplication* uApp = QCoreApplication::instance();
    qInfo() << "APPLICATION: " << uApp->applicationName()
            << "ORGANISATION: " << uApp->organizationName()
            << "VERSION: " << uApp->applicationVersion()
            << "RUNPATH: " << uApp->applicationDirPath();

    qInfo() << "Application ended!";
    uCore::logger::UCoreLogger::deactivateLogging();
}

int UCoreApplication::initializeCoreApplication(int &argc, char **argv[])
{
    //  create only one QCoreApplication or QApplication
    if(!QCoreApplication::instance())
        createApplication(argc, argv);

    s_instance->coreApplication();
    m_qstrlArguments = QCoreApplication::instance()->arguments();

    m_pCommandLineParser = new QCommandLineParser;
    setCommandLineOptions(m_pCommandLineParser);
    if(!m_pCommandLineParser->parse(m_qstrlArguments))
        qCritical() << "Unknown command line options: " << m_pCommandLineParser->unknownOptionNames();
    qDebug() << "CommandLineOptions: " << m_pCommandLineParser->optionNames();
    m_pCommandLineParser->process(m_qstrlArguments);

    if(m_pCommandLineParser->isSet("gui"))
        return s_instance->guiApplication();
    else if(m_pCommandLineParser->isSet("terminal"))
        return s_instance->terminalApplication();
    else
        m_pCommandLineParser->showHelp();
}

void UCoreApplication::createApplication(int &argc, char **argv[]) {
    for(int iArg = 1; iArg < argc; iArg++) {
        if(!qstrcmp((*argv)[iArg], "-gui") ||
           !qstrcmp((*argv)[iArg], "-g") ||
           !qstrcmp((*argv)[iArg], "-G")) {
            new QApplication(argc, *argv);
            return;
        }
    }
    new QCoreApplication(argc, *argv);
}

void UCoreApplication::coreApplication() {
    QCoreApplication* uApp = QCoreApplication::instance();
    uApp->setApplicationName(s_instance->applicationName());
    uApp->setApplicationVersion(s_instance->applicationVersion());
    uApp->setOrganizationName(s_instance->organisationName());

    uCore::logger::UCoreLogger::activateLogging(QDir::currentPath() + QDir::separator() + "log", uCore::logger::ULogLAll, uCore::logger::ULogDNoCategory);

    qDebug() << "Arguments: " << QCoreApplication::instance()->arguments();

    qInfo() << "APPLICATION: " << uApp->applicationName()
            << "ORGANISATION: " << uApp->organizationName()
            << "VERSION: " << uApp->applicationVersion()
            << "RUNPATH: " << uApp->applicationDirPath();

    qInfo() << "Application started!";
}

void UCoreApplication::setCommandLineOptions(QCommandLineParser *parser)
{
    parser->addHelpOption();
    parser->addVersionOption();

    QCommandLineOption qcloGui(QStringList() << "g" << "G" << "gui" << "GUI" << "Gui",
                               tr("Starts application with graphical user interface. Connot be combined with -Terminal.", "CommandLineOption"));
    parser->addOption(qcloGui);

    QCommandLineOption qcloTerminal(QStringList() << "t" << "T" << "terminal" << "TERMINAL" << "Terminal",
                                    tr("Starts application with console interface. Connot be combined with -Terminal.", "CommandLineOption"));
    parser->addOption(qcloTerminal);

    setAdditionalCommandLineOptions(parser);
}

int UCoreApplication::guiApplication() {
    s_instance->startGuiApplication();
    return qApp->exec();
    qInfo() << "End of application";
}

int UCoreApplication::terminalApplication() {
    s_instance->startTerminalApplication();
    m_pTerminal = new terminal::UCoreTerminal(this);
    return QCoreApplication::instance()->exec();
    qInfo() << "End of application";
}
