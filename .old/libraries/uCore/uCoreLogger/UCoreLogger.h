#ifndef UCORELOGGER_H
#define UCORELOGGER_H

#include <QObject>

#include <QDebug>
#include <QDir>

namespace uCore {
namespace logger {

enum ULogLevel{
    ULogLNone               =   0x0000000,
    ULogLDebug              =   0x0000001,
    ULogLInfo               =   0x0000002,
    ULogLWarning            =   0x0000004,
    ULogLCritical           =   0x0000008,
    ULogLFatal              =   0x0000010,

    ULogLCategoryDefault    =   0x8000000,
    ULogLAll                =   0xFFFFFFF
};

enum ULogDetail{
    ULogDNone               =   0x0000000,
    ULogDCategory           =   0x0000001,
    ULogDFile               =   0x0000002,
    ULogDFunction           =   0x0000004,
    ULogDLine               =   0x0000008,

    ULogDNoCategory         =   0xFFFFFFE,
    ULogDAll                =   0xFFFFFFF
};

class UCoreLogger : public QObject {
    Q_OBJECT
public:
    explicit UCoreLogger(QObject *parent = nullptr) : QObject(parent) {}

    static QString activateLogging(QString qstrLogDir = QDir::currentPath() + QDir::separator() + "log", ULogLevel logLevel = ULogLAll, ULogDetail logDetail = ULogDAll);
    static void deactivateLogging();
    static void logMsgHandler(QtMsgType type, const QMessageLogContext &context, const QString &qstrMsg);
    static void setLogLevel(ULogLevel logLevel) { s_logLevel = logLevel; }
    static void setLogDetail(ULogDetail logDetail) { s_logDetail = logDetail; }
    static QString currentLogFile() { return s_qstrCurrentLogFile; }

private:
    static QString getLogFileName();

    //  varialbes
    static bool s_bLogging;
    static QFile* s_qfLogFile;
    static ULogLevel s_logLevel;
    static ULogDetail s_logDetail;
    static QString s_qstrCurrentLogFile;
};

}   //  logger
}   //  uCore

#endif // UCORELOGGER_H
