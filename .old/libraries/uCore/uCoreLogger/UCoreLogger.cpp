#include "UCoreLogger.h"

#include <QDateTime>
#include <math.h>

using namespace uCore::logger;

bool UCoreLogger::s_bLogging = false;
QFile* UCoreLogger::s_qfLogFile = new QFile();
ULogLevel UCoreLogger::s_logLevel = ULogLAll;
ULogDetail UCoreLogger::s_logDetail = ULogDAll;
QString UCoreLogger::s_qstrCurrentLogFile = "";

static const QtMessageHandler QT_DEFAULT_MESSAGE_HANDLER = qInstallMessageHandler(nullptr);

QString UCoreLogger::activateLogging(QString qstrLogDir, ULogLevel logLevel, ULogDetail logDetail)
{
    s_logLevel = logLevel;
    s_logDetail = logDetail;

    QDir qDirLog = qstrLogDir.isEmpty() ? QDir::currentPath() + QDir::separator() + "log" : qstrLogDir;
    if(!qDirLog.exists())
        if(!qDirLog.mkpath(qDirLog.absolutePath()))
            return "1";

    s_qfLogFile->setFileName(qDirLog.filePath(getLogFileName()));
    if(s_qfLogFile->open(QIODevice::Append))
    {
        qInstallMessageHandler(logMsgHandler);
        s_bLogging = true;

        QTextStream qtsLog(s_qfLogFile);
        qtsLog << endl;
        qtsLog.flush();

        qInfo() << QString("Logger activated, LogLevel: 0b%1 LogDetail 0b%2").arg(s_logLevel, 32, 2, QChar('0')).arg(s_logDetail, 32, 2, QChar('0'));
        return "";
    }
    return "2";
}

void UCoreLogger::deactivateLogging()
{
    qInfo() << QString("Logger deactivated, LogLevel: 0b%1 LogDetail 0b%2").arg(s_logLevel, 32, 2, QChar('0')).arg(s_logDetail, 32, 2, QChar('0'));
    s_bLogging = false;
    s_qfLogFile->close();
}

void UCoreLogger::logMsgHandler(QtMsgType type, const QMessageLogContext &context, const QString &qstrMsg)
{
    if(!s_bLogging)
        return (*QT_DEFAULT_MESSAGE_HANDLER)(type, context, qstrMsg);
    if(((1 << static_cast<int>(type)) & s_logLevel) != (1 << static_cast<int>(type)))
        return;

    QTextStream qtsLog(s_qfLogFile);
    qtsLog << QDateTime::currentDateTime().toString(Qt::ISODateWithMs);
    switch (type)
    {
    case QtDebugMsg:    qtsLog << " DBG "; break;
    case QtInfoMsg:     qtsLog << " INF "; break;
    case QtWarningMsg:  qtsLog << " WRN "; break;
    case QtCriticalMsg: qtsLog << " CRT "; break;
    case QtFatalMsg:    qtsLog << " FTL "; break;
    default:
          qWarning() << "Unhandled QtMsgType "
                     << QDateTime::currentDateTime().toString(Qt::ISODateWithMs) << endl
                     << " TYPE:" << static_cast<int>(type) << endl
                     << " CATEGORY: " << context.category << endl
                     << " FILE: " << context.file << endl
                     << " FUNCTION: " << context.function << endl
                     << " LINE: " << context.line << endl
                     << " MSG: " << qstrMsg << endl;
          return (*QT_DEFAULT_MESSAGE_HANDLER)(type, context, qstrMsg);
    }
    qtsLog << "MSG: " << qstrMsg << endl;
    qtsLog.flush();

    if(s_logDetail != ULogDNone)
    {
        qtsLog << "                           ";
        if((s_logDetail & ULogDCategory) == ULogDCategory)
            qtsLog << " CATEGORY: " << context.category;
        if((s_logDetail & ULogDFile) == ULogDFile)
            qtsLog << " FILE: " << context.file;
        if((s_logDetail & ULogDFunction) == ULogDFunction)
            qtsLog << " FUNCTION: " << context.function;
        if((s_logDetail & ULogDLine) == ULogDLine)
            qtsLog << " LINE: " << context.line;
        qtsLog << endl;
        qtsLog.flush();
    }
}

QString UCoreLogger::getLogFileName()
{
    return s_qstrCurrentLogFile = QDateTime::currentDateTime().toString(Qt::ISODate).replace(":", "-") + ".log";
}
