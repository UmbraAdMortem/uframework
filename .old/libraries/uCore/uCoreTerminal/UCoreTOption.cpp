#include "UCoreTOption.h"

#include <QDebug>
#include <QCoreApplication>

using namespace uCore::terminal;

UCoreTOption::UCoreTOption(QString qstrOptionName, QSet<QString> &qsetDefinedOptions, QObject *parent) : QObject(parent)
{
    if(qstrOptionName.isEmpty())
    {
        qCritical() << "No option name specified.";
        QCoreApplication::instance()->exit(-1);
    }
    if(qstrOptionName.size() > MAX_OPTION_LENGTH)
        qWarning() << "Option's name too long.";
    if(qsetDefinedOptions.contains(qstrOptionName))
    {
        qCritical() << "Option's name already used.";
        QCoreApplication::instance()->exit(-1);
    }

    setObjectName(qstrOptionName);

    m_qstrlNameVariations.clear();
    m_qstrlNameVariations.append(qstrOptionName);
    m_qstrlNameVariations.append(qstrOptionName.toUpper());
    m_qstrlNameVariations.append(qstrOptionName.toLower());

    for(int iLetters = 1; iLetters <= qstrOptionName.size(); iLetters++)
    {
        if(qsetDefinedOptions.contains(qstrOptionName.left(iLetters).toLower()))
            continue;
        else
        {
            m_qstrlNameVariations.append(qstrOptionName.left(iLetters).toLower());
            m_qstrlNameVariations.append(qstrOptionName.left(iLetters).toUpper());
            break;
        }
    }
    m_qstrlNameVariations.removeDuplicates();
    foreach(QString qstrName, m_qstrlNameVariations)
        qsetDefinedOptions.insert(qstrName);
}
