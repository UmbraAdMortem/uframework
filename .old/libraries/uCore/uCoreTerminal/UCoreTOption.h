#ifndef UCORETOPTION_H
#define UCORETOPTION_H

#include <QObject>

#include <QSet>

#define MAX_OPTION_LENGTH 20

namespace uCore {
namespace terminal {

class UCoreTOption : public QObject
{
    Q_OBJECT
public:
    explicit UCoreTOption(QString qstrOptionName, QSet<QString> &qsetDefinedOptions, QObject *parent = nullptr);
    bool isOption(const QString &qstrOption) { return m_qstrlNameVariations.contains(qstrOption); }
    QStringList nameVariations() { return m_qstrlNameVariations; }
    void setHelpDescription(const QString& qstrHelpDescription) { m_qstrHelpDescription = qstrHelpDescription; }
    QString helpDescription() { return m_qstrHelpDescription; }

private:
    QStringList m_qstrlNameVariations;
    QString m_qstrHelpDescription;

signals:
    void executeAction(QString qstrCommandRemainder = "");

public slots:
};

}   //  terminal
}   //  uCore

#endif // UCORETOPTION_H
