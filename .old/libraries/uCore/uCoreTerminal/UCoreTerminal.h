#ifndef UCORETERMINAL_H
#define UCORETERMINAL_H

#include <QObject>

#include <QTextStream>
#include <QSet>

#include "UCoreTOption.h"

namespace uCore {
namespace terminal {

class UCoreTerminal : public QObject
{
    Q_OBJECT
public:
    explicit UCoreTerminal(QObject *parent = nullptr);
    ~UCoreTerminal();

private:
    void setDefaultOptions();
    void setCurrentOptions();
    virtual void add2CurrentOptions(QList<UCoreTOption*> &qlCurrentOptions, QSet<QString> &qsetCurrentInputOptions) { Q_UNUSED(qlCurrentOptions) Q_UNUSED(qsetCurrentInputOptions) }

    //  variables
    QTextStream* m_pTerminalInput;
    QTextStream* m_pTerminalOutput;

    QSet<QString> m_qsetDefaultInputOptions;
    QSet<QString> m_qsetCurrentInputOptions;
    QList<UCoreTOption*> m_qlDefaultOptions;
    QList<UCoreTOption*> m_qlCurrentOptions;

    QList<UCoreTOption*> m_qlMenu;
    QList<UCoreTOption*> m_qlAction;

signals:
    void printFinished(QString qstrCommandRemainder = "");
//    void readFromT(QString qstrInput);

public slots:
    void waitOnT(QString qstrInput);
    void print2T(QStringList qstrlOutput = QStringList(), QString qstrCommandRemainder = "");

    void menuSelected(QString qstrCommandRemainder = "");
    void settingSelected(QString qstrCommandRemainder = "");
    void helpSelected(QString qstrCommandRemainder = "");
    void aboutSelected(QString qstrCommandRemainder = "");
    void logSelected(QString qstrCommandRemainder = "");
    void quitSelected(QString qstrCommandRemainder = "");
};

}   //  terminal
}   //  uCore

#endif // UCORETERMINAL_H
