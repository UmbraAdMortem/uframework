#include "UCoreTerminal.h"

#include <QCoreApplication>
#include <QDebug>
#include <QProcess>

using namespace uCore::terminal;

UCoreTerminal::UCoreTerminal(QObject *parent) : QObject(parent)
{
    m_pTerminalInput = new QTextStream(stdin);
    m_pTerminalOutput = new QTextStream(stdout);

    setDefaultOptions();

    setCurrentOptions();

    connect(this, &UCoreTerminal::printFinished,
            this, &UCoreTerminal::waitOnT);

    print2T();
}

UCoreTerminal::~UCoreTerminal()
{

}

void UCoreTerminal::setDefaultOptions()
{
    m_qlDefaultOptions.clear();
    m_qsetDefaultInputOptions.clear();
    UCoreTOption* uctoTemp;

    uctoTemp = new UCoreTOption(tr("Menu", "CommandLine option menu"), m_qsetCurrentInputOptions, this);
    uctoTemp->setHelpDescription(tr("Shows menu of loaded modules", "CommandLine option help"));
    m_qlDefaultOptions.append(uctoTemp);
    m_qsetDefaultInputOptions.unite(QSet<QString>::fromList(uctoTemp->nameVariations()));
    connect(uctoTemp, &UCoreTOption::executeAction,
            this, &UCoreTerminal::menuSelected);

    uctoTemp = new UCoreTOption(tr("Settings", "CommandLine option menu"), m_qsetCurrentInputOptions, this);
    uctoTemp->setHelpDescription(tr("Shows settings of loaded modules", "CommandLine option help"));
    m_qlDefaultOptions.append(uctoTemp);
    m_qsetDefaultInputOptions.unite(QSet<QString>::fromList(uctoTemp->nameVariations()));
    connect(uctoTemp, &UCoreTOption::executeAction,
            this, &UCoreTerminal::settingSelected);

    uctoTemp = new UCoreTOption(tr("Help", "CommandLine option menu"), m_qsetCurrentInputOptions, this);
    uctoTemp->setHelpDescription(tr("Shows this help", "CommandLine option help"));
    m_qlDefaultOptions.append(uctoTemp);
    m_qsetDefaultInputOptions.unite(QSet<QString>::fromList(uctoTemp->nameVariations()));
    connect(uctoTemp, &UCoreTOption::executeAction,
            this, &UCoreTerminal::helpSelected);

    uctoTemp = new UCoreTOption(tr("About", "CommandLine option menu"), m_qsetCurrentInputOptions, this);
    uctoTemp->setHelpDescription(tr("Shows information about this application", "CommandLine option help"));
    m_qlDefaultOptions.append(uctoTemp);
    m_qsetDefaultInputOptions.unite(QSet<QString>::fromList(uctoTemp->nameVariations()));
    connect(uctoTemp, &UCoreTOption::executeAction,
            this, &UCoreTerminal::aboutSelected);

    uctoTemp = new UCoreTOption(tr("Log", "CommandLine option menu"), m_qsetCurrentInputOptions, this);
    uctoTemp->setHelpDescription(tr("Shows content of current logging file", "CommandLine option help"));
    m_qlDefaultOptions.append(uctoTemp);
    m_qsetDefaultInputOptions.unite(QSet<QString>::fromList(uctoTemp->nameVariations()));
    connect(uctoTemp, &UCoreTOption::executeAction,
            this, &UCoreTerminal::logSelected);

    uctoTemp = new UCoreTOption(tr("Quit", "CommandLine option menu"), m_qsetCurrentInputOptions, this);
    uctoTemp->setHelpDescription(tr("Quits application", "CommandLine option help"));
    m_qlDefaultOptions.append(uctoTemp);
    m_qsetDefaultInputOptions.unite(QSet<QString>::fromList(uctoTemp->nameVariations()));
    connect(uctoTemp, &UCoreTOption::executeAction,
            this, &UCoreTerminal::quitSelected);
}

void UCoreTerminal::setCurrentOptions()
{
    m_qsetCurrentInputOptions.clear();
    m_qsetCurrentInputOptions.unite(m_qsetDefaultInputOptions);

    m_qlCurrentOptions.clear();
    m_qlCurrentOptions.append(m_qlDefaultOptions.first());

    add2CurrentOptions(m_qlCurrentOptions, m_qsetCurrentInputOptions);

    for(int iOptions = 1; iOptions < m_qlDefaultOptions.size(); iOptions++)
        m_qlCurrentOptions.append(m_qlDefaultOptions.at(iOptions));
}

void UCoreTerminal::waitOnT(QString qstrInput)
{
    if(qstrInput.isEmpty())
        while (!m_pTerminalInput->readLineInto(&qstrInput)) {
            qDebug() << "Waiting on input";
        }

    bool bOptionExecuted =  false;
    QString qstrCommand = qstrInput.section(" ", 0, 0);

    foreach(UCoreTOption* uctoOption, m_qlCurrentOptions)
        if((bOptionExecuted = uctoOption->isOption(qstrCommand)))
            return emit uctoOption->executeAction(qstrInput.section(" ", 1, -1));

    foreach(UCoreTOption* uctoOption, m_qlAction)
        if((bOptionExecuted = uctoOption->isOption(qstrCommand)))
            return emit uctoOption->executeAction(qstrInput.section(" ", 1, -1));

    foreach(UCoreTOption* uctoOption, m_qlMenu)
        if((bOptionExecuted = uctoOption->isOption(qstrCommand)))
            return emit uctoOption->executeAction(qstrInput.section(" ", 1, -1));

    qWarning() << "Command not recognised: " << qstrCommand;
    helpSelected();
}

void UCoreTerminal::print2T(QStringList qstrlOutput, QString qstrCommandRemainder)
{
    QProcess::execute("clear");
    *m_pTerminalOutput << QCoreApplication::instance()->applicationName().toUpper()
                       << "    -    "
                       << QCoreApplication::instance()->applicationVersion().toUpper()
                       << endl;

    foreach(UCoreTOption* uctoOption, m_qlCurrentOptions)
        *m_pTerminalOutput << uctoOption->objectName().toUpper() << "    ";
    *m_pTerminalOutput << endl;

    //  TODO:   actions

    foreach(QString qstrLine, qstrlOutput)
        *m_pTerminalOutput << qstrLine << endl;


    //  TODO:   status

    emit printFinished(qstrCommandRemainder);
}

void UCoreTerminal::menuSelected(QString qstrCommandRemainder)
{
    if(m_qlMenu.isEmpty())
        print2T(QStringList() << tr("Menu is empty.", "CommandLine error"), qstrCommandRemainder);
}

void UCoreTerminal::settingSelected(QString qstrCommandRemainder)
{
    print2T(QStringList() << tr("Setting is not yet available", "CommandLine error"), qstrCommandRemainder);
}

void UCoreTerminal::helpSelected(QString qstrCommandRemainder)
{
    QStringList qstrlHelp;

    foreach(UCoreTOption* uctoOption, m_qlCurrentOptions)
        qstrlHelp.append(QString("%1 : %2").arg(uctoOption->objectName(), MAX_OPTION_LENGTH).arg(uctoOption->helpDescription()));

    foreach(UCoreTOption* uctoOption, m_qlAction)
        qstrlHelp.append(QString("%1 : %2").arg(uctoOption->objectName(), MAX_OPTION_LENGTH).arg(uctoOption->helpDescription()));

    foreach(UCoreTOption* uctoOption, m_qlMenu)
        qstrlHelp.append(QString("%1 : %2").arg(uctoOption->objectName(), MAX_OPTION_LENGTH).arg(uctoOption->helpDescription()));

    print2T(qstrlHelp, qstrCommandRemainder);
}

void UCoreTerminal::aboutSelected(QString qstrCommandRemainder)
{
    print2T(QStringList() << tr("About is not yet available", "CommandLine error"), qstrCommandRemainder);
}

void UCoreTerminal::logSelected(QString qstrCommandRemainder)
{
    print2T(QStringList() << tr("Log is not yet available", "CommandLine error"), qstrCommandRemainder);
}

void UCoreTerminal::quitSelected(QString qstrCommandRemainder)
{
    Q_UNUSED(qstrCommandRemainder)
    QCoreApplication::instance()->exit();
}
