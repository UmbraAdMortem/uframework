
BASEDIR = $${PWD}
CONFIG(debug, debug | release) {
    LIBTARGET = UCored
    INCLUDEPATH *= $${BASEDIR}/debug
    LIBS *= -L$${BASEDIR}/debug -lUCored
} else:CONFIG(release, debug | release) {
    LIBTARGET = UCore
    INCLUDEPATH += $${BASEDIR}/release
    LIBS *= -L$${BASEDIR}/release -lUCore
}
