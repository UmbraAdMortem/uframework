#include "UBaseApplication.h"

#include <QDebug>

using namespace test::application;

UBaseApplication::UBaseApplication(int &argc, char **argv[], QObject *parent)
    : uCore::application::UCoreApplication(parent) {
     m_iExitCode = initializeCoreApplication(argc, argv);
}

UBaseApplication::~UBaseApplication()
{
    m_pMainWidget->deleteLater();
}

void UBaseApplication::startGuiApplication() {
    m_pMainWidget = new TestMainWindow();
    m_pMainWidget->show();
    qDebug() << "GUI!!!";
}

void UBaseApplication::startTerminalApplication() {
    qDebug() << "Terminal!!!";
}
