#ifndef UBASEAPPLICATION_H
#define UBASEAPPLICATION_H

#include <../uCoreApplication/UCoreApplication.h>

#include "../TestMainWindow.h"

namespace test {
namespace application {

class UBaseApplication : public uCore::application::UCoreApplication
{
    Q_OBJECT
public:
    explicit UBaseApplication(int &argc, char **argv[], QObject *parent = nullptr);
    ~UBaseApplication();
    int exitCode() { return m_iExitCode; }

private:
    QString applicationName() override { return "TestApp"; }
    QString applicationVersion() override { return "00.00.000"; }
    QString organisationName() override { return "UmbraAdMortem"; }

    void startGuiApplication() override;
    void startTerminalApplication() override;

    //  variables
    int m_iExitCode;
    QWidget* m_pMainWidget;
};

}   //  namespace application
}   //  namespace test

#endif // UBASEAPPLICATION_H
