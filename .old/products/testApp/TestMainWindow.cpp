#include "TestMainWindow.h"
#include "ui_TestMainWindow.h"

TestMainWindow::TestMainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::TestMainWindow)
{
    ui->setupUi(this);
}

TestMainWindow::~TestMainWindow()
{
    delete ui;
}

