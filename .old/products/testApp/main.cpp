#include "uBaseApplication/UBaseApplication.h"

int main(int argc, char *argv[]) {
    test::application::UBaseApplication a(argc, &argv);

    return a.exitCode();
}
