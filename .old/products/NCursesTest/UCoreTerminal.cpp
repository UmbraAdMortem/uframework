#include "UCoreTerminal.h"

#include <ncurses.h>

using namespace uCore::terminal;

UCoreTerminal::UCoreTerminal(QObject *parent) : QObject(parent)
{
    int ch;

    initscr();
    raw();
    keypad(stdscr, TRUE);
    noecho();

    printw("Type any character to see it in bold!");
    ch = getch();

    if(ch == KEY_F(1))
        printw("F1 key pressed");
    else
    {
        printw("The pressed key is ");
        attron(A_BOLD);
        printw("%c", ch);
        attroff(A_BOLD);
    }
    refresh();
    getch();
    endwin();
}
