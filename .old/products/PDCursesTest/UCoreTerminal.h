#ifndef UCORETERMINAL_H
#define UCORETERMINAL_H

#include <QObject>

namespace uCore {
namespace terminal {

class UCoreTerminal : public QObject
{
    Q_OBJECT
public:
    explicit UCoreTerminal(QObject *parent = nullptr);

signals:

public slots:
};

}   //  terminal
}   //  uCore

#endif // UCORETERMINAL_H
