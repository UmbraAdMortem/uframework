#include "UFrameWork.h"

#include <QCoreApplication>

#include <UUtilities/ULogger.h>

using namespace uFrameWork;


UFrameWork &UFrameWork::instance()
{
    static UFrameWork instance; // Guaranteed to be destroyed.
                                // Instantiated on first use.
    return instance;
}

UFrameWork::~UFrameWork()
{
    uUtilities::ULogger::instance().deactivateLogging();
    uUtilities::ULogger::instance().deleteLater();
}

int UFrameWork::initFrameWork(int &argc, char **argv[])
{
    Q_UNUSED(argc)
    Q_UNUSED(argv)

    if(m_bInstantiated)
        return -1;
    else
        m_bInstantiated = true;

    uUtilities::ULogger::instance().activateLogging();
    uUtilities::ULogger::instance().setLogDetail(uUtilities::ULogDLine | uUtilities::ULogDFile);

//    new QCoreApplication(argc, *argv);
//    return QCoreApplication::instance()->exec();
    return 0;
}

UFrameWork::UFrameWork()
{
    m_bInstantiated = false;
    m_qstrExtOrgName.clear();
    m_qstrExtAppName.clear();
}
