/*
 * class design as singleton according to
 * https://stackoverflow.com/questions/1008019/c-singleton-design-pattern
 */

#ifndef UFRAMEWORK_H
#define UFRAMEWORK_H

#include "UFrameWork_global.h"

#include <QObject>

namespace uFrameWork {

class UFRAMEWORKSHARED_EXPORT UFrameWork
{

public:
    static UFrameWork& instance();
    ~UFrameWork();

    UFrameWork(UFrameWork const&)       =   delete;
    void operator=(UFrameWork const&)   =   delete;
    // Note: Scott Meyers mentions in his Effective Modern
    //       C++ book, that deleted functions should generally
    //       be public as it results in better error messages
    //       due to the compilers behavior to check accessibility
    //       before deleted status

    QString setExternalOrganisationName(const QString &qstrOrgName) { return m_qstrExtOrgName = qstrOrgName; }
    QString setExternalApplicationName(const QString &qstrAppName) { return m_qstrExtAppName = qstrAppName; }

    int initFrameWork(int &argc, char **argv[]);

private:
    UFrameWork();

//    variables
    bool m_bInstantiated;
    QString m_qstrExtOrgName;
    QString m_qstrExtAppName;
};

} // namespace frameWork

#endif // UFRAMEWORK_H
