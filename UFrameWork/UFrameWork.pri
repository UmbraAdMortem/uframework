
BASEDIR = $${PWD}
CONFIG(debug, debug | release) {
    LIBTARGET = UFrameWorkD
    INCLUDEPATH *= $${BASEDIR}/debug
    LIBS *= -L$${BASEDIR}/debug -lUCored
} else:CONFIG(release, debug | release) {
    LIBTARGET = UFrameWork
    INCLUDEPATH += $${BASEDIR}/release
    LIBS *= -L$${BASEDIR}/release -lUCore
}
