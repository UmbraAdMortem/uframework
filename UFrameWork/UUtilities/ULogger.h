/*
 * getting qDebug to work
 * https://stackoverflow.com/questions/12799653/qdebug-not-displaying-anything
 */

#ifndef ULOGGER_H
#define ULOGGER_H

#include <QObject>

#include <QDebug>
#include <QDir>

namespace uFrameWork
{

namespace uUtilities
{

enum ULogLevel
{
    ULogLNone               =   0x0000000,
    ULogLDebug              =   0x0000001,
    ULogLInfo               =   0x0000002,
    ULogLWarning            =   0x0000004,
    ULogLCritical           =   0x0000008,
    ULogLFatal              =   0x0000010,

    ULogLCategoryDefault    =   0x8000000,
    ULogLAll                =   0xFFFFFFF
};

inline ULogLevel operator|(ULogLevel a, ULogLevel b)
{
    return static_cast<ULogLevel>(static_cast<int>(a) | static_cast<int>(b));
}

enum ULogDetail
{
    ULogDNone               =   0x0000000,
    ULogDCategory           =   0x0000001,
    ULogDFile               =   0x0000002,
    ULogDFunction           =   0x0000004,
    ULogDLine               =   0x0000008,

    ULogDNoCategory         =   0xFFFFFFE,
    ULogDAll                =   0xFFFFFFF
};

inline ULogDetail operator|(ULogDetail a, ULogDetail b)
{
    return static_cast<ULogDetail>(static_cast<int>(a) | static_cast<int>(b));
}

class ULogger : public QObject
{
    Q_OBJECT
public:
    static ULogger& instance();

    ULogger(ULogger const&)         =   delete;
    void operator=(ULogger const&)  =   delete;

    int activateLogging(QDir qDirLog = QDir::currentPath() + QDir::separator() + "log",
                               ULogLevel logLevel = ULogLAll,
                               ULogDetail logDetail = ULogDAll);
    int changeLogDir(QDir qDirLog);
    void deactivateLogging();
    static void logMsgHandler(QtMsgType type, const QMessageLogContext &context, const QString &qstrMsg);
    void setLogLevel(ULogLevel logLevel);
    void setLogDetail(ULogDetail logDetail);
    QString currentLogFile() { return s_qstrCurrentLogFile; }

private:
    ULogger(QObject *parent = nullptr) : QObject(parent) {}

    QString getLogFileName();

    //  varialbes
    static bool s_bLogging;
    static QFile* s_qfLogFile;
    static ULogLevel s_logLevel;
    static ULogDetail s_logDetail;
    static QString s_qstrCurrentLogFile;
};

} // namespace uUtilities

} // namespace uFrameWork

#endif // ULOGGER_H
