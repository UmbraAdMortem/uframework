#include "ULogger.h"

#include <QDateTime>
#include <math.h>

#define ULOG_CONFIG_LENGTH 28
#define ULOG_CONFIG_BASE 2
#define ULOG_CONFIG_FILL QChar('0')

using namespace uFrameWork::uUtilities;

bool ULogger::s_bLogging = false;
QFile* ULogger::s_qfLogFile = new QFile();
ULogLevel ULogger::s_logLevel = ULogLAll;
ULogDetail ULogger::s_logDetail = ULogDAll;
QString ULogger::s_qstrCurrentLogFile = "";

static const QtMessageHandler QT_DEFAULT_MESSAGE_HANDLER = qInstallMessageHandler(nullptr);

ULogger &ULogger::instance()
{
    static ULogger instance;
    return instance;
}

int ULogger::activateLogging(QDir qDirLog, ULogLevel logLevel, ULogDetail logDetail)
{
    s_logLevel = logLevel;
    s_logDetail = logDetail;

    if(!qDirLog.exists())
        if(!qDirLog.mkpath(qDirLog.absolutePath()))
            return -1;

    s_qfLogFile->setFileName(qDirLog.filePath(getLogFileName()));
    if(!s_qfLogFile->open(QIODevice::Append | QIODevice::Text))
        return -2;

    qInstallMessageHandler(logMsgHandler);
    s_bLogging = true;

    QTextStream qtsLog(s_qfLogFile);
    qtsLog << endl;
    qtsLog.flush();

    qDebug() << QString("Logger activated, LogLevel: 0b%1 LogDetail 0b%2")
                .arg(s_logLevel, ULOG_CONFIG_LENGTH, ULOG_CONFIG_BASE, ULOG_CONFIG_FILL)
                .arg(s_logDetail, ULOG_CONFIG_LENGTH, ULOG_CONFIG_BASE, ULOG_CONFIG_FILL);
    return 0;
}

int ULogger::changeLogDir(QDir qDirLog)
{
    qDebug() << QString("Changing LogDirectory to: %1").arg(qDirLog.absolutePath());
    if(!qDirLog.exists())
        if(!qDirLog.mkpath(qDirLog.absolutePath()))
        {
            qWarning() << QString("Could not create LogDirectory: %1").arg(qDirLog.absolutePath());
            qWarning() << QString("LogDirectory unchanged: %1").arg(s_qfLogFile->fileName());
            return -1;
        }

    qDebug() << "Stop logging";
    s_bLogging = false;
    s_qfLogFile->close();
    if(s_qfLogFile->isOpen())
    {
        s_bLogging = true;
        qDebug() << "Continue logging";
        qWarning() << QString("Could not close: %1").arg(s_qfLogFile->fileName());
        qWarning() << QString("LogDirectory unchanged: %1").arg(s_qfLogFile->fileName());
        return -3;
    }

    if(s_qfLogFile->copy(qDirLog.absolutePath() + QDir::separator() + s_qstrCurrentLogFile))
        if(s_qfLogFile->remove())
            s_qfLogFile->setFileName(qDirLog.absolutePath() + QDir::separator() + s_qstrCurrentLogFile);

    if(!s_qfLogFile->open(QIODevice::Append | QIODevice::Text))
        return -2;

    s_bLogging = true;
    qDebug() << "Continue logging";
    qDebug() << QString("LogDirectory changed to: %1").arg(s_qfLogFile->fileName());
    qDebug() << QString("Logger activated, LogLevel: 0b%1 LogDetail 0b%2")
                .arg(s_logLevel, ULOG_CONFIG_LENGTH, ULOG_CONFIG_BASE, ULOG_CONFIG_FILL)
                .arg(s_logDetail, ULOG_CONFIG_LENGTH, ULOG_CONFIG_BASE, ULOG_CONFIG_FILL);
    return 0;
}

void ULogger::deactivateLogging()
{
    qDebug() << QString("Logger deactivated, LogLevel: 0b%1 LogDetail 0b%2")
                .arg(s_logLevel, ULOG_CONFIG_LENGTH, ULOG_CONFIG_BASE, ULOG_CONFIG_FILL)
                .arg(s_logDetail, ULOG_CONFIG_LENGTH, ULOG_CONFIG_BASE, ULOG_CONFIG_FILL);
    s_bLogging = false;
    s_qfLogFile->close();
}

void ULogger::logMsgHandler(QtMsgType type, const QMessageLogContext &context, const QString &qstrMsg)
{
    if(!s_bLogging)
        return (*QT_DEFAULT_MESSAGE_HANDLER)(type, context, qstrMsg);
    if(((1 << static_cast<int>(type)) & s_logLevel) != (1 << static_cast<int>(type)))
        return;

    QTextStream qtsLog(s_qfLogFile);
    qtsLog << QDateTime::currentDateTime().toString(Qt::ISODateWithMs);
    switch (type)
    {
    case QtDebugMsg:    qtsLog << " DBG "; break;
    case QtInfoMsg:     qtsLog << " INF "; break;
    case QtWarningMsg:  qtsLog << " WRN "; break;
    case QtCriticalMsg: qtsLog << " CRT "; break;
    case QtFatalMsg:    qtsLog << " FTL "; break;
    default:
        qWarning() << "Unhandled QtMsgType "
                   << QDateTime::currentDateTime().toString(Qt::ISODateWithMs) << endl
                   << " TYPE:" << static_cast<int>(type) << endl
                   << " CATEGORY: " << context.category << endl
                   << " FILE: " << context.file << endl
                   << " FUNCTION: " << context.function << endl
                   << " LINE: " << context.line << endl
                   << " MSG: " << qstrMsg << endl;
        return (*QT_DEFAULT_MESSAGE_HANDLER)(type, context, qstrMsg);
    }

    if(s_logDetail != ULogDNone)
    {
        if((s_logDetail & ULogDCategory) == ULogDCategory)
            qtsLog << " CATEGORY: " << context.category;
        if((s_logDetail & ULogDFile) == ULogDFile)
            qtsLog << " FILE: " << context.file;
        if((s_logDetail & ULogDFunction) == ULogDFunction)
            qtsLog << " FUNCTION: " << context.function;
        if((s_logDetail & ULogDLine) == ULogDLine)
            qtsLog << " LINE: " << QString("%1 ").arg(context.line, 10);
    }

    qtsLog << " MSG: " << qstrMsg << endl;
    qtsLog.flush();
}

void ULogger::setLogLevel(ULogLevel logLevel)
{
    s_logLevel = logLevel;
    qDebug() << QString("Changing LogLevel to: 0b%1").arg(s_logLevel, ULOG_CONFIG_LENGTH, ULOG_CONFIG_BASE, ULOG_CONFIG_FILL);
}

void ULogger::setLogDetail(ULogDetail logDetail)
{
    s_logDetail = logDetail;
    qDebug() << QString("Changing LogDetail to: 0b%1").arg(s_logDetail, ULOG_CONFIG_LENGTH, ULOG_CONFIG_BASE, ULOG_CONFIG_FILL);
}

QString ULogger::getLogFileName()
{
    return s_qstrCurrentLogFile = QDateTime::currentDateTime().toString(Qt::ISODate).replace(":", "-") + ".log";
}
