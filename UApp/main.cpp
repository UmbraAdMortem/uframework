#include <UFrameWork.h>

int main(int argc, char *argv[])
{
    int iExitCode;
    uFrameWork::UFrameWork* pFrame = &uFrameWork::UFrameWork::instance();
    pFrame->setExternalApplicationName("UApp");
    pFrame->setExternalOrganisationName("UAM");
    iExitCode = pFrame->initFrameWork(argc, &argv);
    return iExitCode;
}
